package com.ishan.demo.fanficreader.activities.story;

import android.graphics.Canvas;
import android.text.StaticLayout;
import android.util.Log;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ishan_000 on 03-Jun-15.
 */
public class TextViewPaginator implements Serializable {
    private List<TextViewPage> textViewPages;
    private TextView textView;
    private String text;
    private int pageCount;
    private int currentPage;

    public TextViewPaginator(TextView textView, String text) {
        this(textView, text, 0);
    }

    public TextViewPaginator(TextView textView, String text, int startPage) {
        this.textView = textView;
        this.text = text;
        this.currentPage = startPage;

        populatePages();
    }

    public void populatePages() {
        textViewPages = new ArrayList<>();

        int textViewWidth = textView.getMeasuredWidth() - textView.getPaddingLeft() - textView.getPaddingRight();
        int textViewHeight = textView.getMeasuredHeight() - textView.getPaddingTop() - textView.getPaddingBottom();

        StaticLayout layout = new StaticLayout(text, textView.getPaint(), textViewWidth, textView.getLayout().getAlignment(), textView.getLineSpacingMultiplier(), textView.getLineSpacingExtra(), false);
        layout.draw(new Canvas());

        int bottomLine = layout.getLineForVertical(textViewHeight);
        int lineInterval = bottomLine;
        int lineCount = layout.getLineCount();
        for (int i = 0, topLine = 0; topLine < lineCount ; i++, topLine += lineInterval, bottomLine += lineInterval) {
            int pageOffset = layout.getLineStart(topLine);

            int pageEnd;
            try {
                pageEnd = layout.getLineEnd(bottomLine);
            } catch (Exception e) {
                pageEnd = text.length();
            }

            if (pageEnd == 0) {
                pageEnd = text.length();
            }
            if (pageEnd < pageOffset) break;
            textViewPages.add(new TextViewPage(i, text.substring(pageOffset, pageEnd), topLine, bottomLine));
        }
        pageCount = textViewPages.size();
        showPage(currentPage);
    }

    public void showPage(int pageNo) {
        textView.setText(getPage(pageNo).getText());
    }

    public int getPageCount() {
        return pageCount;
    }

    public void next() {
        showPage(currentPage + 1);
    }

    public void previous() {
        showPage(currentPage - 1);
    }

    public TextViewPage getPage(int pageNo) {
        if (pageNo > -1 && pageNo < pageCount) {
            currentPage = pageNo;
        }
        return textViewPages.get(currentPage);
    }

    public void setTextView(TextView textView){
        this.textView = textView;
        this.textView.post(new Runnable() {
            @Override
            public void run() {
                populatePages();
            }
        });
    }
}
