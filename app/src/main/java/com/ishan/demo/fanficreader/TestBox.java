package com.ishan.demo.fanficreader;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ishan_000 on 01-Jun-15.
 */
public class TestBox {
    public static void main(String[] args) throws IOException {
//        Document document = Jsoup.connect("https://www.fanfiction.net/book/Harry-Potter/").get();
        int a = (int) Math.ceil((float)1 / 3);
        System.out.println(a);
    }

    public static Matcher regexSearch(String str, String regex) {
        Pattern r = Pattern.compile(regex);
        return r.matcher(str);
    }

    public static String regexSearch(String str, String regex, int index) {
        Matcher m = regexSearch(str, regex);
        return m.find() ? m.group(index) : "";
    }
}
