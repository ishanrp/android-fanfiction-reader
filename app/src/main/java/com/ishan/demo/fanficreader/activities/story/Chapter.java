package com.ishan.demo.fanficreader.activities.story;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.ishan.demo.fanficreader.models.Story;

import java.io.Serializable;

/**
 * Created by ishan_000 on 03-Jun-15.
 */

@Table(name = "chapters")
public class Chapter extends Model implements Serializable {
    @Column(name = "chapter_id")
    private int chapterId;

    @Column(name = "title")
    private String title;

    @Column(name = "content")
    private String content;

    @Column(name = "story")
    private Story story;

    public Chapter() {
    }

    public Chapter(int chapterId, String title, Story story) {
        this(chapterId, title, story, "");
    }

    public Chapter(int chapterId, String title, Story story, String content) {
        this.chapterId = chapterId;
        this.title = title;
        this.story = story;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getChapterId() {
        return chapterId;
    }

    public void setChapterId(int chapterId) {
        this.chapterId = chapterId;
    }

    @Override
    public String toString() {
        return title;
    }

    public Story getStory() {
        return story;
    }

    public void setStory(Story story) {
        this.story = story;
    }
}
