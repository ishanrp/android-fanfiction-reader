package com.ishan.demo.fanficreader.models;

/**
 * Created by ishan_000 on 31-May-15.
 */
public class ImageListItem {
    private int id;
    private String text;
    private int imageId;

    public ImageListItem(int id, String text, int imageId) {
        this.id = id;
        this.text = text;
        this.imageId = imageId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}
