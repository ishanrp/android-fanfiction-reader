package com.ishan.demo.fanficreader.activities.subcategory;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;

import com.ishan.demo.fanficreader.models.Category;
import com.ishan.demo.fanficreader.models.SubCategoryItem;
import com.ishan.demo.fanficreader.service.AsyncNetTaskService;
import com.ishan.demo.fanficreader.service.FanfictionService;
import com.ishan.demo.fanficreader.service.FanfictionServiceImpl;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ishan_000 on 31-May-15.
 */
public class SubCategoriesFragment extends ListFragment implements SearchView.OnQueryTextListener {
    public static final String ARG_CATEGORY_TYPE = "ARG_CATEGORY_TYPE";
    public static final String ARG_CATEGORY = "ARG_CATEGORY";
    public static final String ARG_LOAD_LIMIT = "ARG_LOAD_LIMIT";
    public static final String KEY_SUB_CATEGORY_ITEM_LIST = "KEY_SUB_CATEGORY_ITEM_LIST";

    private Callbacks callbacks;
    private List<SubCategoryItem> subCategoryItemList;
    private ArrayAdapter<SubCategoryItem> subCategoryItemArrayAdapter;

    public static SubCategoriesFragment newInstance(Category category, int categoryType) {
        SubCategoriesFragment fragment = new SubCategoriesFragment();

        Bundle args = new Bundle();
        args.putSerializable(ARG_CATEGORY, category);
        args.putInt(ARG_CATEGORY_TYPE, categoryType);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        callbacks = (Callbacks) activity;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        subCategoryItemArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, new ArrayList<SubCategoryItem>());
        if (savedInstanceState == null) {
            Category category = (Category) getArguments().getSerializable(ARG_CATEGORY);
            int categoryType = getArguments().getInt(ARG_CATEGORY_TYPE);
            int loadCount = getArguments().getInt(ARG_LOAD_LIMIT, 20);

            populateList(category, categoryType, loadCount);
        } else {
            subCategoryItemList = (List<SubCategoryItem>) savedInstanceState.getSerializable(KEY_SUB_CATEGORY_ITEM_LIST);
            refreshListAdapter();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable(KEY_SUB_CATEGORY_ITEM_LIST, (Serializable) subCategoryItemList);
    }

    public void populateList(Category category, int categoryType, final int loadCount) {
        FanfictionService fanfictionService = FanfictionServiceImpl.getInstance(getActivity());

        fanfictionService.getSubCategories(category.getUri(), categoryType, new FanfictionServiceImpl.Callbacks() {
            @Override
            public void onTaskCompletion(Bundle results) {
                subCategoryItemList = (List<SubCategoryItem>) results.getSerializable(RESULT_SUB_CATEGORY_ITEM_LIST);
                refreshListAdapter();
            }
        });
    }

    private void refreshListAdapter() {
        subCategoryItemArrayAdapter.addAll(subCategoryItemList);
        setListAdapter(subCategoryItemArrayAdapter);
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (subCategoryItemArrayAdapter != null) {
            subCategoryItemArrayAdapter.getFilter().filter(newText);
        }
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        SubCategoryItem subCategoryItem = subCategoryItemArrayAdapter.getItem(position);
        callbacks.onSubCategoryItemClick(subCategoryItem);
    }

    interface Callbacks {
        void onSubCategoryItemClick(SubCategoryItem subCategoryItem);
    }
}
