package com.ishan.demo.fanficreader.intent;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.ishan.demo.fanficreader.activities.story.Chapter;
import com.ishan.demo.fanficreader.models.Story;
import com.ishan.demo.fanficreader.service.FanfictionService;
import com.ishan.demo.fanficreader.service.FanfictionServiceImpl;

/**
 * Created by ishan_000 on 05-Jun-15.
 */
public class DownloaderIntent extends IntentService {

    public static final String INTENT_EXTRA_STORY = "INTENT_EXTRA_STORY";
    private FanfictionService fanfictionService;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     */
    public DownloaderIntent() {
        super("DownloaderIntent");

        fanfictionService = FanfictionServiceImpl.getInstance(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final Story story = (Story) intent.getSerializableExtra(INTENT_EXTRA_STORY);
        story.setOnline(false);

        try {
            story.save();
            ActiveAndroid.beginTransaction();
            for (final Chapter chapter : story.getChapterList()) {
                if (chapter.getContent().isEmpty()) {
                    fanfictionService.getStoryChapter(story, chapter.getChapterId(), false, new FanfictionServiceImpl.Callbacks() {
                        @Override
                        public void onTaskCompletion(Bundle results) {
                            Chapter tempChapter = new Chapter(chapter.getChapterId(), chapter.getTitle(), story, results.getString(RESULT_STORY_BODY));
                            tempChapter.save();
                        }
                    });
                } else {
                    chapter.save();
                }
            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
            Log.d("fan1", "Download complete!!");
        }
    }
}
