package com.ishan.demo.fanficreader.adapters;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.Arrays;
import java.util.List;

/**
 * Created by ishan_000 on 05-Jun-15.
 */
public class CustomArrayAdapter<T> extends ArrayAdapter<T> {
    private List<T> items;

    public CustomArrayAdapter(Context context, int resource, T[] items) {
        super(context, resource, items);

        this.items = Arrays.asList(items);
    }

    public CustomArrayAdapter(Context context, int resource, List<T> items) {
        super(context, resource, items);

        this.items = items;
    }

    public List<T> getItems() {
        return items;
    }
}
