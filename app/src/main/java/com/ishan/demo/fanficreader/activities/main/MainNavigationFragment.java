package com.ishan.demo.fanficreader.activities.main;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.ishan.demo.fanficreader.adapters.ImageListAdapter;
import com.ishan.demo.fanficreader.models.ImageListItem;

import java.util.List;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainNavigationFragment extends ListFragment {
    private Callbacks callbacks;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        callbacks = (Callbacks) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ImageListAdapter imageListAdapter = new ImageListAdapter(getActivity(), callbacks.getItems());
        setListAdapter(imageListAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        callbacks.onListItemClick(position);
    }

    interface Callbacks {
        List<ImageListItem> getItems();
        void onListItemClick(int position);
    }
}
