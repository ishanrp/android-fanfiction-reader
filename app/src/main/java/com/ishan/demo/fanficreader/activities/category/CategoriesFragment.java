package com.ishan.demo.fanficreader.activities.category;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.ishan.demo.fanficreader.R;
import com.ishan.demo.fanficreader.models.Category;

/**
 * Created by ishan_000 on 31-May-15.
 */
public class CategoriesFragment extends Fragment implements TabLayout.OnTabSelectedListener, AdapterView.OnItemClickListener {
    private Callbacks fragmentListener;
    private int categoryType = 0;

    private static final Category[] CATEGORIES = {
            new Category("Anime/Manga", "anime"),
            new Category("Books", "book"),
            new Category("Cartoons", "cartoon"),
            new Category("Comics", "comic"),
            new Category("Games", "game"),
            new Category("Misc", "misc"),
            new Category("Movies", "movie"),
            new Category("Plays/Musicals", "play"),
            new Category("TV Shows", "tv")
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        fragmentListener = (Callbacks) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category_list, container, false);
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tlCategories);
        ListView lvCategories = (ListView) view.findViewById(R.id.lvCategories);

        tabLayout.addTab(tabLayout.newTab().setText(R.string.regular_category));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.crossover_category));

        ArrayAdapter <Category> arrayAdapter = new ArrayAdapter<Category>(getActivity(), android.R.layout.simple_list_item_1, CATEGORIES);
        lvCategories.setAdapter(arrayAdapter);

        tabLayout.setOnTabSelectedListener(this);
        lvCategories.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        categoryType = tab.getPosition();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        fragmentListener.onCategoryItemClick(CATEGORIES[position], categoryType);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public interface Callbacks {
        void onCategoryItemClick(Category category, int categoryType);
    }
}
