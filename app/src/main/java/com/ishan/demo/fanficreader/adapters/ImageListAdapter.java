package com.ishan.demo.fanficreader.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ishan.demo.fanficreader.R;
import com.ishan.demo.fanficreader.models.ImageListItem;

import java.util.List;

/**
 * Created by ishan_000 on 31-May-15.
 */
public class ImageListAdapter extends ArrayAdapter<ImageListItem> {
    private final static int layoutResourceId = R.layout.image_list_row;
    private Activity context;
    private List<ImageListItem> items;

    public ImageListAdapter(Context context, List<ImageListItem> objects) {
        super(context, layoutResourceId, objects);
        this.context = (Activity) context;
        items = objects;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View row = convertView;
        ImageListAdapter.MenuItemHolder holder = null;
        if(row == null)
        {
            LayoutInflater inflater = context.getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new MenuItemHolder();
            holder.txtTitle = (TextView)row.findViewById(R.id.tvTitle);
            holder.imgIcon = (ImageView)row.findViewById(R.id.ivIcon);

            row.setTag(holder);
        }
        else
        {
            holder = (ImageListAdapter.MenuItemHolder)row.getTag();
        }

        ImageListItem menuRow = getItem(position);
        holder.txtTitle.setText(menuRow.getText());
        holder.imgIcon.setImageResource(menuRow.getImageId());

        return row;
    }

    /**
     * A cache of the ImageView and the TextView. Provides a speed improvement.
     * @author Michael Chen
     */
    private static class MenuItemHolder
    {
        private ImageView imgIcon;
        private TextView txtTitle;
    }
}
