package com.ishan.demo.fanficreader.models;

import java.io.Serializable;

/**
 * Created by ishan_000 on 01-Jun-15.
 */
public class FilterOption implements Serializable {
    private String key;
    private String label;

    public FilterOption(String key, String label) {
        this.key = key;
        this.label = label;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
