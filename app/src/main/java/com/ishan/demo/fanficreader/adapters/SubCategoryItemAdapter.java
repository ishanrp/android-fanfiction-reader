package com.ishan.demo.fanficreader.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ishan.demo.fanficreader.R;
import com.ishan.demo.fanficreader.models.SubCategoryItem;

import java.util.List;

/**
 * Created by ishan_000 on 31-May-15.
 */
public class SubCategoryItemAdapter extends ArrayAdapter<SubCategoryItem> {
    private static final int layoutResourceId = R.layout.fragment_sub_category_list_row;
    private List<SubCategoryItem> items;
    private Activity activity;

    public SubCategoryItemAdapter(Context context, List<SubCategoryItem> objects) {
        super(context, layoutResourceId, objects);
        activity = (Activity) context;
        items = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        SubCategoryItemAdapter.MenuItemHolder holder = null;
        if(row == null)
        {
            LayoutInflater inflater = activity.getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new MenuItemHolder();
            holder.txtLabel = (TextView)row.findViewById(R.id.tvLabel);
            holder.txtStoryCount = (TextView) row.findViewById(R.id.tvStoryCount);

            row.setTag(holder);
        }
        else
        {
            holder = (SubCategoryItemAdapter.MenuItemHolder)row.getTag();
        }

        SubCategoryItem menuRow = getItem(position);
        holder.txtLabel.setText(menuRow.getLabel());
        holder.txtStoryCount.setText(String.valueOf(menuRow.getCount()));

        return row;
    }

    /**
     * A cache of the ImageView and the TextView. Provides a speed improvement.
     * @author Michael Chen
     */
    private static class MenuItemHolder {
        private TextView txtLabel;
        private TextView txtStoryCount;
    }
}
