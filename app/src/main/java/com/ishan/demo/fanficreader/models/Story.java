package com.ishan.demo.fanficreader.models;

import android.util.Log;

import com.activeandroid.Cache;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.ishan.demo.fanficreader.activities.story.Chapter;
import com.ishan.demo.fanficreader.utils.GetterUtil;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by ishan_000 on 01-Jun-15.
 */
@Table(name = "story")
public class Story extends Model implements Serializable {
    @Column(name = "story_id")
    private String storyId;

    @Column(name = "title")
    private String title;

    @Column(name = "author")
    private String author;

    @Column(name = "author_id")
    private String authorId;

    @Column(name = "genre")
    private String genre;

    @Column(name = "uri")
    private String url;

    @Column(name = "description")
    private String description;

    @Column(name = "published_on")
    private Date publishedOn;

    @Column(name = "updated_on")
    private Date updatedOn;

    @Column(name = "reviews")
    private int reviews;

    @Column(name = "follows")
    private int follows;

    @Column(name = "favorites")
    private int favorites;

    @Column(name = "word_count")
    private int wordCount;

    @Column(name = "words")
    private String words;

    @Column(name = "characters")
    private String characters;

    @Column(name = "rating")
    private String rating;

    @Column(name = "language")
    private String language;

    @Column(name = "chapters")
    private int chapters;

    @Column(name = "status")
    private int status;

    @Column(name = "online")
    private boolean online;

    @Column(name = "modified_on")
    private Date modifiedOn;

    @Column(name = "created_on")
    private Date createdOn;

//    @Column(name = "category")
    private SubCategoryItem category;

    //    @Column(name = "chapters")
    private List<Chapter> chapterList;

    @Column(name = "chapter_in_progress")
    private int chanpterInProgress;

    @Column(name = "current_page_position")
    private int currentPagePosition;

    public Story() {
    }

    public Story(String storyId, String title, String author, String url, String description) {
        this.storyId = storyId;
        this.title = title;
        this.author = author;
        this.url = url;
        this.description = description;
    }

    public void setPublishedOn(Date publishedOn) {
        this.publishedOn = publishedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }


    public SubCategoryItem getCategory() {
        return category;
    }

    public void setCategory(SubCategoryItem category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getPublishedOn() {
        return publishedOn;
    }

    public void setPublishedOn(long publishedOn) {
        this.publishedOn = new Date(publishedOn);
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(long updatedOn) {
        this.updatedOn = new Date(updatedOn);
    }

    public int getReviews() {
        return reviews;
    }

    public void setReviews(int reviews) {
        this.reviews = reviews;
    }

    public int getFollows() {
        return follows;
    }

    public void setFollows(int follows) {
        this.follows = follows;
    }

    public int getFavorites() {
        return favorites;
    }

    public void setFavorites(int favorites) {
        this.favorites = favorites;
    }

    public String getCharacters() {
        return characters;
    }

    public void setCharacters(String characters) {
        this.characters = characters;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getChapters() {
        return chapters;
    }

    public void setChapters(int chapters) {
        this.chapters = chapters;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public int getWordCount() {
        return wordCount;
    }

    public void setWordCount(int wordCount) {
        this.wordCount = wordCount;
    }

    public String getWords() {
        return words;
    }

    public void setWords(String words) {
        setWordCount(GetterUtil.parseInt(words, 0, true));
        this.words = words;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStoryId() {
        return storyId;
    }

    public void setStoryId(String storyId) {
        this.storyId = storyId;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public int getChanpterInProgress() {
        return chanpterInProgress;
    }

    public int getNextChapter() {
        int nextChapterNo = getChanpterInProgress() + 1;
        return nextChapterNo <= getChapters() ? nextChapterNo : -1;
    }

    public int getPreviousChapter() {
        int previousChapterNo = getChanpterInProgress() - 1;
        return previousChapterNo > 0 ? previousChapterNo : -1;
    }

    public void setChanpterInProgress(int chanpterInProgress) {
        this.chanpterInProgress = chanpterInProgress;
    }

    public List<Chapter> getChapterList() {
        if (chapterList == null) {
            chapterList = chapters();
        }
        return chapterList;
    }

    public void setChapterList(List<Chapter> chapterList) {
        this.chapterList = chapterList;
    }

    public void addChapter(Chapter chapter) {
        chapterList.add(chapter);
    }

    public Chapter getCurrentChapter() {
        return findChapterById(getChanpterInProgress());
    }

    public Chapter getChapterById(int id) {
        return getChapterList().get(id - 1);
    }

    public Chapter setChapterBody(int id, String body) {
        Chapter chapter = getChapterById(id);
        chapter.setContent(body);
        return chapter;
    }

    public int getCurrentPagePosition() {
        return currentPagePosition;
    }

    public void setCurrentPagePosition(int currentPagePosition) {
        this.currentPagePosition = currentPagePosition;
    }

    private List<Chapter> chapters() {
        try {
            return new Select("Id","chapter_id", "title", "story").from(Chapter.class).where(Cache.getTableName(Chapter.class) + ".story=?", getId()).execute();
        } catch (Exception e) {
            Log.e("fan", e.getMessage());
        }
        return null;
    }

    public Chapter findChapterById(int chapterId) {
        return new Select()
                .from(Chapter.class)
                .where("story = ? and chapter_id = ?", getId(), chapterId)
                .executeSingle();
    }

    public static Story findByStoryId(String storyId) {
        return new Select()
                .from(Story.class)
                .where("story_id = ?", storyId)
                .executeSingle();
    }
}
