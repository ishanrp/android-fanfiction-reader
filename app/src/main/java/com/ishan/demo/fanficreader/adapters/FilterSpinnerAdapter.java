package com.ishan.demo.fanficreader.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ishan.demo.fanficreader.R;
import com.ishan.demo.fanficreader.models.Filter;
import com.ishan.demo.fanficreader.models.FilterOption;

/**
 * Created by ishan_000 on 01-Jun-15.
 */
public class FilterSpinnerAdapter extends ArrayAdapter<FilterOption> {
    private final static int layoutResourceId = R.layout.filter_spinner_item;
    private Activity context;
    private Filter item;

    public FilterSpinnerAdapter(Context context, Filter object) {
        super(context, layoutResourceId, object.getOptions());
        this.context = (Activity) context;
        this.item = object;
    }

    public Filter getFilterItem() {
        return item;
    }

    public FilterOption getFilterOption(int position) {
        return getItem(position);
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        FilterSpinnerAdapter.MenuItemHolder holder = null;
        if(row == null)
        {
            LayoutInflater inflater = context.getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new MenuItemHolder();
            holder.text1 = (TextView)row.findViewById(android.R.id.text1);

            row.setTag(holder);
        }
        else
        {
            holder = (FilterSpinnerAdapter.MenuItemHolder)row.getTag();
        }

        FilterOption menuRow = getItem(position);
        holder.text1.setText(menuRow.getLabel());

        return row;
    }

    /**
     * A cache of the ImageView and the TextView. Provides a speed improvement.
     * @author Michael Chen
     */
    private static class MenuItemHolder {
        private TextView text1;
    }
}
