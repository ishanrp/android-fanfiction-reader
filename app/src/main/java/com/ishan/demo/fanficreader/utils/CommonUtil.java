package com.ishan.demo.fanficreader.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ishan_000 on 01-Jun-15.
 */
public class CommonUtil {

    public static Matcher regexSearch(String str, String regex) {
        Pattern r = Pattern.compile(regex);
        return r.matcher(str);
    }

    public static String regexSearch(String str, String regex, int index) {
        Matcher m = regexSearch(str, regex);
        return m.find() ? m.group(index) : "";
    }
}
