package com.ishan.demo.fanficreader.service.model;

import java.util.Map;

/**
 * Created by ishan_000 on 31-May-15.
 */
public class Request {
    private String url;
    private RequestMethod method;
    private Map<String, String> params;

    public Request(String url, RequestMethod method) {
        this.url = url;
        this.method = method;
        this.params = null;
    }

    public Request(String url, RequestMethod method, Map<String, String> params) {
        this.url = url;
        this.method = method;
        this.params = params;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public RequestMethod getMethod() {
        return method;
    }

    public void setMethod(RequestMethod method) {
        this.method = method;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }
}
