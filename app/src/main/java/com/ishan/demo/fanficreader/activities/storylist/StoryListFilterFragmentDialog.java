package com.ishan.demo.fanficreader.activities.storylist;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.ishan.demo.fanficreader.R;
import com.ishan.demo.fanficreader.adapters.FilterSpinnerAdapter;
import com.ishan.demo.fanficreader.models.Filter;
import com.ishan.demo.fanficreader.models.FilterSpinner;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by ishan_000 on 01-Jun-15.
 */
public class StoryListFilterFragmentDialog extends DialogFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    public static final String ARG_FILTERS = "ARG_FILTERS";
    public static final String ARG_CURRENTLY_SELECTED_FILTERS = "ARG_CURRENTLY_SELECTED_FILTERS";
    public static final String TAG_STORY_LIST_FILTER_FRAGMENT_DIALOG = "TAG_STORY_LIST_FILTER_FRAGMENT_DIALOG";
    Callbacks callbacks;
    LinearLayout relativeLayout;

    private static final FilterSpinner[] FILTER_SPINNERS = {
            new FilterSpinner("sortid"          , "srt"   , R.id.sp_sortid),
            new FilterSpinner("timerange"       , "t"     , R.id.sp_timerange),
            new FilterSpinner("genreid1"        , "g1"    , R.id.sp_genreid1),
            new FilterSpinner("genreid2"        , "g2"    , R.id.sp_genreid2),
            new FilterSpinner("censorid"        , "r"     , R.id.sp_censorid),
            new FilterSpinner("languageid"      , "lan"   , R.id.sp_languageid),
            new FilterSpinner("length"          , "len"   , R.id.sp_length),
            new FilterSpinner("statusid"        , "s"     , R.id.sp_statusid),
            new FilterSpinner("verseid1"        , "v1"    , R.id.sp_verseid1),
            new FilterSpinner("characterid1"    , "c1"    , R.id.sp_characterid1),
            new FilterSpinner("characterid2"    , "c2"    , R.id.sp_characterid2),
            new FilterSpinner("characterid3"    , "c3"    , R.id.sp_characterid3),
            new FilterSpinner("characterid4"    , "c4"    , R.id.sp_characterid4),
            new FilterSpinner("_genreid1"       , "_g1"   , R.id.sp__genreid1),
            new FilterSpinner("_verseid1"       , "_v1"   , R.id.sp__verseid1),
            new FilterSpinner("_characterid1"   , "_c1"   , R.id.sp__characterid1),
            new FilterSpinner("_characterid2"   , "_c2"   , R.id.sp__characterid2)
    };

    public static StoryListFilterFragmentDialog newInstance(Map<String, Filter> filters, Map<String, Integer> currentlySelectedFilters) {
        StoryListFilterFragmentDialog fragment = new StoryListFilterFragmentDialog();

        Bundle args = new Bundle();
        args.putSerializable(ARG_FILTERS, (Serializable) filters);
        args.putSerializable(ARG_CURRENTLY_SELECTED_FILTERS, (Serializable) currentlySelectedFilters);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        callbacks = (Callbacks) activity;
    }

//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        Dialog dialog = super.onCreateDialog(savedInstanceState);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        return dialog;
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Map<String, Filter> filters = (Map<String, Filter>) getArguments().getSerializable(ARG_FILTERS);
        Map<String, Integer> currentFilters = (Map<String, Integer>) getArguments().getSerializable(ARG_CURRENTLY_SELECTED_FILTERS);
        View view = inflater.inflate(R.layout.linear_filter_dialog, container, false);

        relativeLayout = (LinearLayout) view.findViewById(R.id.ll_dialog);
        setStyle(DialogFragment.STYLE_NO_TITLE, getTheme());

        Button button = (Button) view.findViewById(R.id.bApply);
        button.setOnClickListener(this);

        button = (Button) view.findViewById(R.id.bCancel);
        button.setOnClickListener(this);

        getDialog().setTitle(R.string.filter);

        populateSpinners(filters, currentFilters);
        return view;
    }

    private void populateSpinners(Map<String, Filter> filters, Map<String, Integer> currentFilters) {
        FilterSpinnerAdapter filterSpinnerAdapter;
        Spinner spinner;
        Filter temp;
        Integer current;

        for (FilterSpinner f : FILTER_SPINNERS) {
            spinner = (Spinner) relativeLayout.findViewById(f.getSpinnerId());
            temp = filters.get(f.getName());
            if (temp != null) {
                temp.setParam(f.getParameter());
                filterSpinnerAdapter = new FilterSpinnerAdapter(getActivity(), temp);
                spinner.setAdapter(filterSpinnerAdapter);
                spinner.setOnItemSelectedListener(this);
                current = currentFilters.get(f.getParameter());
                if (current != null) {
                    spinner.setSelection(current);
                }
            } else {
                spinner.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bApply:
                callbacks.filterStories();

            case R.id.bCancel:
                getDialog().dismiss();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        FilterSpinnerAdapter filterSpinnerAdapter = (FilterSpinnerAdapter) parent.getAdapter();
        callbacks.addFilter(filterSpinnerAdapter.getFilterItem().getParam(), filterSpinnerAdapter.getItem(position).getKey(), position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    interface Callbacks {
        void addFilter(String key, String value, int position);

        void filterStories();
    }
}
