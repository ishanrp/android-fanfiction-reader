package com.ishan.demo.fanficreader.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ishan.demo.fanficreader.R;
import com.ishan.demo.fanficreader.models.Story;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by ishan_000 on 01-Jun-15.
 */
public class StoryListAdapter extends ArrayAdapter<Story> {
    private final static int layoutResourceId = R.layout.fragment_story_detail_row;
    private Activity context;
    private List<Story> stories;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.US);

    public StoryListAdapter(Context context, List<Story> objects) {
        super(context, layoutResourceId, objects);
        this.context = (Activity) context;
        stories = objects;
    }

//    @Override
//    public long getItemId(int position) {
//        return getItem(position).getId();
//    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View row = convertView;
        StoryListAdapter.MenuItemHolder holder = null;
        if(row == null)
        {
            LayoutInflater inflater = context.getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new MenuItemHolder();
            holder.title = (TextView)row.findViewById(R.id.tvTitle);
            holder.author = (TextView)row.findViewById(R.id.tvAuthor);
            holder.description = (TextView)row.findViewById(R.id.tvDescription);
            holder.words = (TextView)row.findViewById(R.id.tvWords);
            holder.chapters = (TextView)row.findViewById(R.id.tvChapters);
            holder.updatedOn = (TextView)row.findViewById(R.id.tvUpdated);
            holder.rating = (TextView)row.findViewById(R.id.tvRating);

            row.setTag(holder);
        }
        else
        {
            holder = (StoryListAdapter.MenuItemHolder)row.getTag();
        }

        Story menuRow = getItem(position);
        holder.title.setText(menuRow.getTitle());
        holder.author.setText(menuRow.getAuthor());
        holder.description.setText(menuRow.getDescription());
        holder.chapters.setText(String.valueOf(menuRow.getChapters()));
        holder.words.setText(String.valueOf(menuRow.getWords()));
        holder.rating.setText(menuRow.getRating());
        Date date = menuRow.getUpdatedOn();
        if (date == null) {
            date = menuRow.getPublishedOn();
        }
        holder.updatedOn.setText(simpleDateFormat.format(date));
        if (menuRow.getStatus() == 0) {
            holder.title.setTextColor(Color.WHITE);
        } else {
            holder.title.setTextColor(Color.GREEN);
        }

        return row;
    }

    public List<Story> getStories() {
        return stories;
    }

    /**
     * A cache of the ImageView and the TextView. Provides a speed improvement.
     * @author Michael Chen
     */
    private static class MenuItemHolder {
        private TextView title;
        private TextView author;
        private TextView description;
        private TextView words;
        private TextView chapters;
        private TextView publishedOn;
        private TextView updatedOn;
        private TextView characters;
        private TextView rating;
    }
}
