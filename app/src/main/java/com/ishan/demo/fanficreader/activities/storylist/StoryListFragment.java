package com.ishan.demo.fanficreader.activities.storylist;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.ishan.demo.fanficreader.adapters.StoryListAdapter;
import com.ishan.demo.fanficreader.models.Filter;
import com.ishan.demo.fanficreader.models.Story;
import com.ishan.demo.fanficreader.models.SubCategoryItem;
import com.ishan.demo.fanficreader.service.AsyncNetTaskService;
import com.ishan.demo.fanficreader.service.FanfictionService;
import com.ishan.demo.fanficreader.service.FanfictionServiceImpl;
import com.ishan.demo.fanficreader.utils.CommonUtil;
import com.ishan.demo.fanficreader.utils.GetterUtil;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

/**
 * Created by ishan_000 on 01-Jun-15.
 */
public class StoryListFragment extends ListFragment {
    public static final String ARG_SUB_CATEGORY_ITEM = "ARG_SUB_CATEGORY_ITEM";
    public static final String FRAGMENT_TAG = "STORY_LIST_FRAGMENT_TAG";
    public static final String KEY_FILTERS_AVAILABLE = "KEY_FILTERS_AVAILABLE";
    public static final String KEY_SUB_CATEGORY_ITEM = "KEY_SUB_CATEGORY_ITEM";
    private static final String KEY_STORY_DETAILS = "KEY_STORY_DETAILS";

    private Callable callable;

    private FanfictionService fanfictionService;
    private Map<String, Filter> filtersAvailable;
    private SubCategoryItem subCategoryItem;
    StoryListAdapter storyListAdapter;

    public static StoryListFragment newInstance(SubCategoryItem subCategoryItem) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_SUB_CATEGORY_ITEM, subCategoryItem);

        StoryListFragment fragment = new StoryListFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        callable = (Callable) activity;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        List<Story> stories = new ArrayList<>();
        fanfictionService = FanfictionServiceImpl.getInstance(getActivity());
        storyListAdapter = new StoryListAdapter(getActivity(), stories);

        if (savedInstanceState == null) {
            filtersAvailable = new HashMap<>();
            subCategoryItem = (SubCategoryItem) getArguments().getSerializable(ARG_SUB_CATEGORY_ITEM);

            filterStories(0, null, false);
        } else {
            filtersAvailable = (Map<String, Filter>) savedInstanceState.getSerializable(KEY_FILTERS_AVAILABLE);
            subCategoryItem = (SubCategoryItem) savedInstanceState.getSerializable(KEY_SUB_CATEGORY_ITEM);
            stories = (List<Story>) savedInstanceState.getSerializable(KEY_STORY_DETAILS);
            setStoryListAdapter(stories);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(KEY_FILTERS_AVAILABLE, (Serializable) filtersAvailable);
        outState.putSerializable(KEY_SUB_CATEGORY_ITEM, subCategoryItem);
        outState.putSerializable(KEY_STORY_DETAILS, (Serializable) storyListAdapter.getStories());
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Story story = storyListAdapter.getItem(position);
        story.setCategory(subCategoryItem);
        callable.onStoryClick(story);
    }

    public void filterStories(int page, Map<String, String> filters, final boolean shoulClear) {
        final boolean populateFilters = filtersAvailable == null || filtersAvailable.isEmpty();
        fanfictionService.getStories(subCategoryItem.getUri(), page, filters, populateFilters, new FanfictionServiceImpl.Callbacks() {
            @Override
            public void onTaskCompletion(Bundle results) {
                List<Story> stories = (List<Story>) results.getSerializable(RESULT_STORY_LIST);
                if (shoulClear) {
                    clearStoryListAdapter();
                }

                setStoryListAdapter(stories);
                if (populateFilters) {
                    filtersAvailable = (Map<String, Filter>) results.getSerializable(RESULT_STORY_FILTER_LIST);
                }
            }
        });
    }

    public void showFilterDialog(Map<String, Integer> currentFiltersPositions) {
        FragmentManager fragmentManager = getFragmentManager();
        StoryListFilterFragmentDialog newFragment = StoryListFilterFragmentDialog.newInstance(filtersAvailable, currentFiltersPositions);
        newFragment.show(fragmentManager, StoryListFilterFragmentDialog.TAG_STORY_LIST_FILTER_FRAGMENT_DIALOG);
    }

    private void clearStoryListAdapter() {
        storyListAdapter.clear();
    }

    private void setStoryListAdapter(List<Story> stories) {
        storyListAdapter.addAll(stories);
        setListAdapter(storyListAdapter);
    }

    interface Callable {
        void onStoryClick(Story story);
    }
}
