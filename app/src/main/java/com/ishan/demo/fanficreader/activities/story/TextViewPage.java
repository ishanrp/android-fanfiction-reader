package com.ishan.demo.fanficreader.activities.story;

import java.io.Serializable;

/**
 * Created by ishan_000 on 03-Jun-15.
 */
public class TextViewPage implements Serializable {
    private int index;
    private String text;
    private int startLine;
    private int endLine;

    public TextViewPage(int index, String text, int startLine, int endLine) {
        this.index = index;
        this.text = text;
        this.startLine = startLine;
        this.endLine = endLine;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getStartLine() {
        return startLine;
    }

    public void setStartLine(int startLine) {
        this.startLine = startLine;
    }

    public int getEndLine() {
        return endLine;
    }

    public void setEndLine(int endLine) {
        this.endLine = endLine;
    }
}
