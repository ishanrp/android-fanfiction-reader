package com.ishan.demo.fanficreader.service.model;

/**
 * Created by ishan_000 on 31-May-15.
 */
public enum RequestMethod {
    GET, POST
}
