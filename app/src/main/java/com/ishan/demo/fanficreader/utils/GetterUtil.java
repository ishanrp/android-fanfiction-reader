package com.ishan.demo.fanficreader.utils;

/**
 * Created by ishan_000 on 01-Jun-15.
 */
public class GetterUtil {
    public static int parseInt(String s) {
        return parseInt(s, 0, false);
    }

    public static int parseInt(String s, int def, boolean replace) {
        int val = def;
        try {
            if (replace) {
                s = s.replace(",", "");
            }
            val = Integer.parseInt(s);
        } catch (NumberFormatException e) {
        }
        return val;
    }
}
