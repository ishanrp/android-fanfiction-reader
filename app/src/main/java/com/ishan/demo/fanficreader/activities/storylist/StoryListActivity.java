package com.ishan.demo.fanficreader.activities.storylist;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.ishan.demo.fanficreader.R;
import com.ishan.demo.fanficreader.activities.story.StoryReaderActivity;
import com.ishan.demo.fanficreader.models.Story;
import com.ishan.demo.fanficreader.models.SubCategoryItem;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ishan_000 on 01-Jun-15.
 */
public class StoryListActivity extends AppCompatActivity implements StoryListFragment.Callable, StoryListFilterFragmentDialog.Callbacks {
    public static final String EXTRA_SUB_CATEGORY_ITEM = "EXTRA_SUB_CATEGORY_ITEM";
    public static final String KEY_CURRENT_FILTER_POSITIONS = "KEY_CURRENT_FILTER_POSITIONS";
    public static final String KEY_CURRENT_FILTER_VALUES = "KEY_CURRENT_FILTER_VALUES";

    private StoryListFragment storyListFragment;
    private Map<String, String> currentFiltersValues;
    private Map<String, Integer> currentFiltersPositions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_story_list);

        Bundle extras = getIntent().getExtras();

        storyListFragment = (StoryListFragment) getFragmentManager().findFragmentByTag(StoryListFragment.FRAGMENT_TAG);
        if (storyListFragment == null) {
            SubCategoryItem subCategoryItem = (SubCategoryItem) extras.getSerializable(EXTRA_SUB_CATEGORY_ITEM);
            storyListFragment = StoryListFragment.newInstance(subCategoryItem);
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container, storyListFragment, StoryListFragment.FRAGMENT_TAG);
            transaction.commit();

            currentFiltersValues = new HashMap<>();
            currentFiltersPositions = new HashMap<>();
        } else {
            currentFiltersValues = (Map<String, String>) savedInstanceState.getSerializable(KEY_CURRENT_FILTER_VALUES);
            currentFiltersPositions = (Map<String, Integer>) savedInstanceState.getSerializable(KEY_CURRENT_FILTER_POSITIONS);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable(KEY_CURRENT_FILTER_VALUES, (Serializable) currentFiltersValues);
        outState.putSerializable(KEY_CURRENT_FILTER_POSITIONS, (Serializable) currentFiltersPositions);
    }

    @Override
    public void filterStories() {
        storyListFragment.filterStories(0, currentFiltersValues, true);
    }

    @Override
    public void addFilter(String key, String value, int position) {
        currentFiltersValues.put(key, value);
        currentFiltersPositions.put(key, position);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.story_list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_filter:
                storyListFragment.showFilterDialog(currentFiltersPositions);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStoryClick(Story story) {
        Intent intent = new Intent(getApplicationContext(), StoryReaderActivity.class);
        intent.putExtra(StoryReaderActivity.EXTRA_STORY_DETAILS, story);
        startActivity(intent);
    }
}
