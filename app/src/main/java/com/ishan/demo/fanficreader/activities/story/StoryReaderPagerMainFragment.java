package com.ishan.demo.fanficreader.activities.story;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.ishan.demo.fanficreader.R;
import com.ishan.demo.fanficreader.adapters.CustomPagerAdapter;
import com.ishan.demo.fanficreader.intent.DownloaderIntent;
import com.ishan.demo.fanficreader.models.Story;
import com.ishan.demo.fanficreader.service.FanfictionService;
import com.ishan.demo.fanficreader.service.FanfictionServiceImpl;


import java.util.List;

/**
 * Created by ishan_000 on 03-Jun-15.
 */
public class StoryReaderPagerMainFragment extends Fragment implements View.OnClickListener {
    public static final String ARG_STORY_DETAIL = "ARG_STORY_DETAIL";
    public static final String FRAGMENT_TAG = "StoryReaderPagerMainFragmentTAG";
    public static final String EXTRA_STORY = "EXTRA_STORY";

    private ViewPager viewPager;
    private ProgressDialog progressDialog;
    private FanfictionService fanfictionService;

    private CustomPagerAdapter mPagerAdapter;
    private boolean fullscreenState;
    private Story story;

    public static StoryReaderPagerMainFragment newInstance(Story story) {
        StoryReaderPagerMainFragment fragment = new StoryReaderPagerMainFragment();

        Bundle args = new Bundle();
        args.putSerializable(ARG_STORY_DETAIL, story);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_story_reader_viewpager, container, false);

        fanfictionService = FanfictionServiceImpl.getInstance(getActivity());
        viewPager = (ViewPager) view.findViewById(R.id.pager);
        initProgressDialog();
        fullscreenState = false;

        if (savedInstanceState == null) {
            final Story tempStory = (Story) getArguments().getSerializable(ARG_STORY_DETAIL);
            viewPager.post(new Runnable() {
                @Override
                public void run() {

                    story = Story.findByStoryId(tempStory.getStoryId());
                    if (story == null) {
                        story = tempStory;
                        story.setChanpterInProgress(1);
                        loadStoryChapterFromOnline(story.getChanpterInProgress());
                    } else {
                        loadStoryFromDb();
                    }
                }
            });
        } else {
            story = (Story) savedInstanceState.getSerializable(EXTRA_STORY);
            mPagerAdapter = new CustomPagerAdapter(getChildFragmentManager(), story.getCurrentChapter().getContent());
            viewPager.setAdapter(mPagerAdapter);
            mPagerAdapter.notifyDataSetChanged();
        }

        initListeners(view);
        toggleFullscreen();

        return view;
    }

    public void loadStoryChapterFromOnline(final int chapterNo) {
        progressDialog.show();
        final boolean loadChapters = story.getChapterList() == null;
        if (!loadChapters) {
            Chapter chapter = story.findChapterById(chapterNo);
            if (chapter != null) {
                setCurrentChapter(chapter, true);
                return;
            }
        }
        fanfictionService.getStoryChapter(story, chapterNo, loadChapters, new FanfictionServiceImpl.Callbacks() {
            @Override
            public void onTaskCompletion(Bundle results) {
                String chapterBody = results.getString(RESULT_STORY_BODY);
                if (loadChapters) {
                    List<Chapter> chapterList = (List<Chapter>) results.getSerializable(RESULT_STORY_CHAPTER_LIST);
                    story.setChapterList(chapterList);
                }
                setCurrentChapter(story.setChapterBody(chapterNo, chapterBody), true);
            }
        });
    }

    private void loadStoryFromDb() {
        setCurrentChapter(story.getCurrentChapter(), false);
    }

    private void setCurrentChapter(Chapter chapter, boolean resetPostion) {
        story.setChanpterInProgress(chapter.getChapterId());

        if (resetPostion) {
            story.setCurrentPagePosition(0);
        }
        mPagerAdapter = new CustomPagerAdapter(getChildFragmentManager(), chapter.getContent());
        viewPager.setAdapter(mPagerAdapter);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(EXTRA_STORY, story);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bNext:
                nextPage();
                break;
            case R.id.bPrev:
                previousPage();
                break;
            case R.id.toggleFullscreen:
                toggleFullscreen();
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (!story.isOnline()) {
            story.save();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_story_reader, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.show_chapter_list_dialog:
                showChapterListDialog();
                return true;
            case R.id.action_download_story:
                downloadStory();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void downloadStory() {
        Intent downloadIntent = new Intent(getActivity(), DownloaderIntent.class);
        downloadIntent.putExtra(DownloaderIntent.INTENT_EXTRA_STORY, story);
        getActivity().startService(downloadIntent);
        Toast.makeText(getActivity(), "Download started!!", Toast.LENGTH_SHORT).show();
    }

    public void showChapterListDialog() {
        FragmentManager fragmentManager = getFragmentManager();
        StoryReaderDialogFragment newFragment = StoryReaderDialogFragment.newInstance(story.getChapterList());
        newFragment.show(fragmentManager, StoryReaderDialogFragment.TAG_STORY_READER_FRAGMENT_DIALOG);
    }

    public void setPaginator(TextViewPaginator paginator) {
        mPagerAdapter.setPaginator(paginator);
        setViewPagerPage(story.getCurrentPagePosition());
        progressDialog.dismiss();
    }

    private void initListeners(View view) {
        Button bNext = (Button) view.findViewById(R.id.bNext);
        Button bPrev = (Button) view.findViewById(R.id.bPrev);
        Button toggleFullscreen = (Button) view.findViewById(R.id.toggleFullscreen);

        bNext.setOnClickListener(this);
        bPrev.setOnClickListener(this);
        toggleFullscreen.setOnClickListener(this);

        getActivity().getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                refreshPages();
            }
        });
    }

    private void toggleFullscreen() {
        if (fullscreenState) {
            showSystemUI();
        } else {
            hideSystemUI();
        }
        fullscreenState = !fullscreenState;
    }

    private void nextPage() {
        setViewPagerPage(viewPager.getCurrentItem() + 1);
    }

    private void previousPage() {
        setViewPagerPage(viewPager.getCurrentItem() - 1);
    }

    private void setViewPagerPage(int pageNo) {
        if (pageNo < 0) {
            loadPreviousChapter();
        } else if (mPagerAdapter.getCount() > pageNo) {
            viewPager.setCurrentItem(pageNo);
            story.setCurrentPagePosition(pageNo);
        } else {
            loadNextChapter();
        }
    }

    private void loadPreviousChapter() {
        int previous = story.getPreviousChapter();
        if (previous > 0) {
            loadStoryChapterFromOnline(previous);
        }
    }

    private void loadNextChapter() {
        int next = story.getNextChapter();
        if (next > 0) {
            loadStoryChapterFromOnline(next);
        }
    }

    private void refreshPages() {
//        if (paginator != null) paginator.populatePages();
    }

    // This snippet hides the system bars.
    private void hideSystemUI() {
        getActivity().getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    private void showSystemUI() {
        getActivity().getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    private void initProgressDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setMax(100);
    }
}
