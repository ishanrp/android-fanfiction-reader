package com.ishan.demo.fanficreader.activities.subcategory;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.SearchView;

import com.ishan.demo.fanficreader.R;
import com.ishan.demo.fanficreader.activities.storylist.StoryListActivity;
import com.ishan.demo.fanficreader.models.Category;
import com.ishan.demo.fanficreader.models.SubCategoryItem;

/**
 * Created by ishan_000 on 31-May-15.
 */
public class SubCategoriesActivity extends AppCompatActivity implements SubCategoriesFragment.Callbacks {
    public static final String EXTRA_CATEGORY = "EXTRA_CATEGORY";
    public static final String EXTRA_CATEGORY_TYPE = "EXTRA_CATEGORY_TYPE";

    private SubCategoriesFragment subCategoriesFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category_list);

        Bundle extras = getIntent().getExtras();
        if (savedInstanceState == null && extras != null) {
            Category category = (Category) extras.getSerializable(EXTRA_CATEGORY);
            int categoryType = extras.getInt(EXTRA_CATEGORY_TYPE);

            subCategoriesFragment = SubCategoriesFragment.newInstance(category, categoryType);
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container, subCategoriesFragment);
            transaction.commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.sub_category_menu, menu);
        SearchView mSearchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        mSearchView.setOnQueryTextListener(subCategoriesFragment);
        return true;
    }

    @Override
    public void onSubCategoryItemClick(SubCategoryItem subCategoryItem) {
        Intent intent = new Intent(getApplicationContext(), StoryListActivity.class);
        intent.putExtra(StoryListActivity.EXTRA_SUB_CATEGORY_ITEM, subCategoryItem);
        startActivity(intent);
    }
}
