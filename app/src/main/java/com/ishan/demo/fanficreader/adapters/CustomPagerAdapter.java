package com.ishan.demo.fanficreader.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import com.ishan.demo.fanficreader.activities.story.StoryReaderPagerFragment;
import com.ishan.demo.fanficreader.activities.story.TextViewPage;
import com.ishan.demo.fanficreader.activities.story.TextViewPaginator;

/**
 * Created by ishan_000 on 03-Jun-15.
 */
public class CustomPagerAdapter extends FragmentStatePagerAdapter {
    private TextViewPaginator paginator;
    private String text;
    private int currentPage;
    private int baseId = 0;

    public CustomPagerAdapter(FragmentManager fm, String text) {
        this(fm, text, 0);
    }

    public CustomPagerAdapter(FragmentManager fm, String text, int currentPage) {
        super(fm);

        this.text = text;
        this.currentPage = currentPage;
    }

    @Override
    public Fragment getItem(int position) {
        TextViewPage textViewPage = paginator != null ? paginator.getPage(position) : null;
        return StoryReaderPagerFragment.newInstance(textViewPage, position, text);
    }

    @Override
    public int getCount() {
        if (paginator != null) {
            return paginator.getPageCount();
        }
        return 5;
    }

    @Override
    public int getItemPosition(Object object) {
        // refresh all fragments when data set changed
        return PagerAdapter.POSITION_NONE;
    }

    public void setPaginator(TextViewPaginator paginator) {
        this.paginator = paginator;
        notifyDataSetChanged();
    }

    public String getText() {
        return text;
    }

}
