package com.ishan.demo.fanficreader.activities.story;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.ishan.demo.fanficreader.R;
import com.ishan.demo.fanficreader.adapters.CustomArrayAdapter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ishan_000 on 02-Jun-15.
 */
public class StoryReaderDialogFragment extends DialogFragment implements AdapterView.OnItemClickListener {
    private static final String ARG_STORY_CHAPTER_LIST = "ARG_STORY_CHAPTER_LIST";
    public static final String TAG_STORY_READER_FRAGMENT_DIALOG = "TAG_STORY_READER_FRAGMENT_DIALOG";
    private StoryReaderDialogFragment.Callbacks callbacks;

    public static StoryReaderDialogFragment newInstance(List<Chapter> chapterList) {
        StoryReaderDialogFragment fragment = new StoryReaderDialogFragment();

        Bundle args = new Bundle();
        args.putSerializable(ARG_STORY_CHAPTER_LIST, (Serializable) chapterList);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        callbacks = (Callbacks) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_chapter_list, container, false);
        List<Chapter> chapterList = (List<Chapter>) getArguments().getSerializable(ARG_STORY_CHAPTER_LIST);

        ListView listView = (ListView) view.findViewById(R.id.list);
        CustomArrayAdapter<Chapter> chapterCustomArrayAdapter = new CustomArrayAdapter<Chapter>(getActivity(), android.R.layout.simple_list_item_1, chapterList);
        listView.setAdapter(chapterCustomArrayAdapter);

        listView.setOnItemClickListener(this);

        getDialog().setTitle(R.string.chapter_dialog_title);

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        callbacks.onChanpterClickListener((Chapter) parent.getAdapter().getItem(position));
        getDialog().dismiss();
    }

    interface Callbacks {
        void onChanpterClickListener(Chapter chapter);
    }
}
