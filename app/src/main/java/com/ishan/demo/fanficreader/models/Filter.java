package com.ishan.demo.fanficreader.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ishan_000 on 01-Jun-15.
 */
public class Filter implements Serializable {
    private String label;
    private String name;
    private String param;
    private List<FilterOption> options;

    public Filter(String label, String name) {
        this.label = label;
        this.name = name;
        options = new ArrayList<>();
    }

    public Filter(String label, String name, List<FilterOption> options) {
        this.label = label;
        this.name = name;
        this.options = options;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FilterOption> getOptions() {
        return options;
    }

    public void setOptions(List<FilterOption> options) {
        this.options = options;
    }

    public void addOption(String key, String value) {
        options.add(new FilterOption(key, value));
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }
}
