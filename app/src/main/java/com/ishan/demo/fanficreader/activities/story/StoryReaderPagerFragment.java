package com.ishan.demo.fanficreader.activities.story;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ishan.demo.fanficreader.R;

/**
 * Created by ishan_000 on 03-Jun-15.
 */
public class StoryReaderPagerFragment extends Fragment {

    private static final String ARG_PAGE = "ARG_PAGE";
    private static final String ARG_PAGE_NO = "ARG_PAGE_NO";
    private static final String ARG_TEXT = "ARG_TEXT";
    private Callbacks callbacks;

    public static StoryReaderPagerFragment newInstance(TextViewPage textViewPage, int pageNo, String text) {
        StoryReaderPagerFragment fragment = new StoryReaderPagerFragment();

        Bundle args = new Bundle();
        args.putSerializable(ARG_PAGE, textViewPage);
        args.putString(ARG_TEXT, text);
        args.putInt(ARG_PAGE_NO, pageNo);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        callbacks = (Callbacks) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_story_reader_pager, container, false);

        final TextViewPage textViewPage = (TextViewPage) getArguments().getSerializable(ARG_PAGE);
        final String text = getArguments().getString(ARG_TEXT);
        final int pageNo = getArguments().getInt(ARG_PAGE_NO);
        final TextView textView = (TextView) rootView.findViewById(R.id.story);

        textView.post(new Runnable() {
            @Override
            public void run() {
                if (textViewPage == null) {
                    TextViewPaginator paginator = new TextViewPaginator(textView, text, pageNo);
                    callbacks.setPaginator(paginator);
                } else {
                    textView.setText(textViewPage.getText());
                }
            }
        });


        return rootView;
    }

    interface Callbacks {
        void setPaginator(TextViewPaginator paginator);
    }
}
