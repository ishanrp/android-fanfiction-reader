package com.ishan.demo.fanficreader.service;

import com.ishan.demo.fanficreader.models.Category;
import com.ishan.demo.fanficreader.models.Story;
import com.ishan.demo.fanficreader.models.SubCategoryItem;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by ishan_000 on 31-May-15.
 */
public interface FanfictionService extends Serializable {
    void getSubCategories(String uri, int categoryType, FanfictionServiceImpl.Callbacks callbacks);
    void getStories(String url, int page, Map<String, String> filters, boolean populateFilters, FanfictionServiceImpl.Callbacks callbacksListener);
    void getStoryChapter(Story story, int chapterNo, final boolean needChapterList, FanfictionServiceImpl.Callbacks callbacksListener);
    boolean checkInternetConnectivity();
}
