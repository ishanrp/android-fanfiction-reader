package com.ishan.demo.fanficreader.service;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Html;

import com.ishan.demo.fanficreader.activities.story.Chapter;
import com.ishan.demo.fanficreader.models.Filter;
import com.ishan.demo.fanficreader.models.Story;
import com.ishan.demo.fanficreader.models.SubCategoryItem;
import com.ishan.demo.fanficreader.service.model.Request;
import com.ishan.demo.fanficreader.service.model.RequestMethod;
import com.ishan.demo.fanficreader.utils.CommonUtil;
import com.ishan.demo.fanficreader.utils.GetterUtil;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

/**
 * Created by ishan_000 on 31-May-15.
 */
public class FanfictionServiceImpl implements FanfictionService {
    private static final String BASE_URI = "https://www.fanfiction.net/";
    private static final String CROSSOVER_URI = BASE_URI + "crossovers/";
    private static final String STORY_URI = BASE_URI + "s/";
    private static final String FORWARD_SLASH = "/";
    private ConnectivityManager connectivityManager;

    public FanfictionServiceImpl() {
    }

    private FanfictionServiceImpl(ConnectivityManager connectivityManager) {
        this.connectivityManager = connectivityManager;
    }

    public static FanfictionServiceImpl getInstance(Context context) {
        return new FanfictionServiceImpl(/*(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)*/);
    }

    @Override
    public void getSubCategories(String uri, int categoryType, final Callbacks callbacksListener) {
        String url;
        if (categoryType == 0) {
            url = BASE_URI + uri;
        } else {
            url = CROSSOVER_URI + uri + FORWARD_SLASH;
        }
        executeAsyncRequest(new Request(url, RequestMethod.GET), new AsyncNetTaskService.AsyncTaskListener() {
            @Override
            public void onTaskCompletion(Document document) {
                List<SubCategoryItem> subCategoryItemList = new ArrayList<>();
                Elements elements = document.select("#list_output td > div");
                String url, label, count;
                Element anchor, element;
                int size = elements.size();

                for (int i = 0; i < size; i++) {
                    element = elements.get(i);
                    anchor = element.select("a").first();
                    url = anchor.attr("abs:href");
                    label = anchor.ownText();
                    count = element.select("span").first().ownText();
                    subCategoryItemList.add(new SubCategoryItem(i, label, url, count));
                }

                Bundle bundle = new Bundle();
                bundle.putSerializable(Callbacks.RESULT_SUB_CATEGORY_ITEM_LIST, (Serializable) subCategoryItemList);
                callbacksListener.onTaskCompletion(bundle);
            }
        });
    }

    @Override
    public void getStories(String url, int page, Map<String, String> filters, final boolean populateFilters, final Callbacks callbacksListener) {
        if (page > 0) {
            if (filters == null) {
                filters = new HashMap<>();
            }
            filters.put("p", String.valueOf(page));
        }
        executeAsyncRequest(new Request(url, RequestMethod.GET, filters), new AsyncNetTaskService.AsyncTaskListener() {
            @Override
            public void onTaskCompletion(Document document) {
                Elements elements = document.select("div.z-list");
                List<Story> stories = new ArrayList<>();
                Story story;
                Element tempElement;
                Elements spans;
                Matcher matcher;
                Bundle bundle = new Bundle();

                for (Element element : elements) {
                    story = new Story();

                    tempElement = element.child(0);
                    story.setTitle(tempElement.text());
                    story.setUrl(tempElement.absUrl("href"));

                    matcher = CommonUtil.regexSearch(tempElement.absUrl("href"), "/s/(\\d+)/");
                    if (matcher.find( )) {
                        story.setStoryId(matcher.group(1));
                    }

                    tempElement = element.select("a[href^=/u/]").first();
                    story.setAuthor(tempElement.ownText());
                    matcher = CommonUtil.regexSearch(tempElement.absUrl("href"), "/u/(\\d+)/");
                    if (matcher.find( )) {
                        story.setAuthorId(matcher.group(1));
                    }

                    tempElement = element.getElementsByClass("z-padtop").first();
                    story.setDescription(tempElement.ownText());

                    tempElement  = tempElement.getElementsByClass("z-padtop2").first();
                    String txt = tempElement.ownText();

                    matcher = CommonUtil.regexSearch(txt, "Rated: (\\S+) - (\\S+) - (\\S+) - Chapters: (\\d+) - Words: ([\\d,]+)");
                    if (!matcher.find()) {
                        matcher = CommonUtil.regexSearch(txt, "Rated: (\\S+) - (\\S+) (-) Chapters: (\\d+) - Words: ([\\d,]+)");
                    }
                    matcher.reset();
                    if (matcher.find()) {
                        story.setRating(matcher.group(1));
                        story.setLanguage(matcher.group(2));
                        story.setGenre(matcher.group(3));
                        story.setChapters(GetterUtil.parseInt(matcher.group(4)));
                        story.setWords(matcher.group(5));
                    }

                    story.setReviews(GetterUtil.parseInt(CommonUtil.regexSearch(txt, "Reviews: (\\d+)", 1), 0, true));
                    story.setFavorites(GetterUtil.parseInt(CommonUtil.regexSearch(txt, "Favs: (\\d+)", 1), 0, true));
                    story.setFollows(GetterUtil.parseInt(CommonUtil.regexSearch(txt, "Follows: (\\d+)", 1), 0, true));
                    story.setStatus(txt.contains(" - Complete") ? 1 : 0);

                    spans = tempElement.getElementsByTag("span");
                    if (spans.size() == 2) {
                        story.setUpdatedOn(Long.parseLong(spans.first().attr("data-xutime")) * 1000);
                        story.setPublishedOn(Long.parseLong(spans.last().attr("data-xutime")) * 1000);
                    } else {
                        story.setPublishedOn(Long.parseLong(spans.first().attr("data-xutime")) * 1000);
                    }
                    story.setOnline(true);

                    stories.add(story);
                }

                bundle.putSerializable(Callbacks.RESULT_STORY_LIST, (Serializable) stories);

                if (populateFilters) {
                    elements = document.select("#filters #myform select");
                    Filter filter;
                    Map<String, Filter> filterMap = new HashMap<String, Filter>();

                    for (Element element : elements) {
                        filter = new Filter(element.attr("title"), element.attr("name"));

                        for (Element op : element.getElementsByTag("option")) {
                            filter.addOption(op.attr("value"), op.ownText());
                        }
                        filterMap.put(filter.getName(), filter);
                    }
                    bundle.putSerializable(Callbacks.RESULT_STORY_FILTER_LIST, (Serializable) filterMap);
                }
                callbacksListener.onTaskCompletion(bundle);
            }
        });
    }

    @Override
    public void getStoryChapter(final Story story, int chapterNo, final boolean needChapterList, final Callbacks callbacksListener) {
        String url = STORY_URI + story.getStoryId() + FORWARD_SLASH + chapterNo;

        executeAsyncRequest(new Request(url, RequestMethod.GET), /*callbacksListener,*/ new AsyncNetTaskService.AsyncTaskListener() {
            @Override
            public void onTaskCompletion(Document document) {
                Element element = document.getElementById("storytext");
                String storyBody = Html.fromHtml(element.html()).toString();
                Bundle bundle = new Bundle();
                List<Chapter> chapterList;
                Chapter chapter;

                bundle.putString(Callbacks.RESULT_STORY_BODY, storyBody);
                if (needChapterList) {
                    chapterList  = new ArrayList<>();
                    for (Element option : document.select("#chap_select option")) {
                        int valChapter = Integer.parseInt(option.val());
                        chapter = new Chapter(valChapter, option.ownText(), story);
                        chapterList.add(chapter);
                    }
                    if (chapterList.isEmpty()) {
                        chapterList.add(new Chapter(1, "Chapter 1", story));
                    }
                    bundle.putSerializable(Callbacks.RESULT_STORY_CHAPTER_LIST, (Serializable) chapterList);
                }
                callbacksListener.onTaskCompletion(bundle);
            }
        });
    }

    public boolean checkInternetConnectivity() {
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    private void executeAsyncRequest(Request request, /*Callbacks callbacksListener, */AsyncNetTaskService.AsyncTaskListener asyncTaskListener) {
//        if(checkInternetConnectivity()) {
            AsyncNetTaskService asyncNetTaskService = new AsyncNetTaskService(asyncTaskListener);
            asyncNetTaskService.execute(request);
//        } else {
//            callbacksListener.onError("Network connection error");
//        }
    }

    public interface Callbacks {
        String RESULT_STORY_BODY = "RESULT_STORY_BODY";
        String RESULT_STORY_CHAPTER_LIST = "RESULT_STORY_CHAPTER_LIST";
        String RESULT_STORY_LIST = "RESULT_STORY_LIST";
        String RESULT_STORY_FILTER_LIST = "RESULT_STORY_FILTER_LIST";
        String RESULT_SUB_CATEGORY_ITEM_LIST = "RESULT_SUB_CATEGORY_ITEM_LIST";
        void onTaskCompletion(Bundle results);
//        void onError(String msg);
    }
}
