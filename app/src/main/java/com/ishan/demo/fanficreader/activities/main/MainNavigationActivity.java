package com.ishan.demo.fanficreader.activities.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.ishan.demo.fanficreader.R;
import com.ishan.demo.fanficreader.models.ImageListItem;

import java.util.Arrays;
import java.util.List;


public class MainNavigationActivity extends AppCompatActivity implements MainNavigationFragment.Callbacks {
    private static final ImageListItem [] NAVIGATION_LIST_ITEMS = new ImageListItem[]{
        new ImageListItem(0, "My Library", R.drawable.ic_storage),
        new ImageListItem(1, "Categories", R.drawable.ic_storage),
        new ImageListItem(2, "Search", R.drawable.ic_storage),
        new ImageListItem(3, "Settings", R.drawable.ic_storage)
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_navigation);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public List<ImageListItem> getItems() {
        return Arrays.asList(NAVIGATION_LIST_ITEMS);
    }

    @Override
    public void onListItemClick(int position) {
        Log.d("####FAN", "position: " + position);
    }
}
