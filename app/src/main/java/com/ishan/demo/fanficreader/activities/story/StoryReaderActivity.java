package com.ishan.demo.fanficreader.activities.story;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;

import com.ishan.demo.fanficreader.R;
import com.ishan.demo.fanficreader.models.Story;

/**
 * Created by ishan_000 on 02-Jun-15.
 */
public class StoryReaderActivity extends AppCompatActivity implements StoryReaderPagerFragment.Callbacks, StoryReaderDialogFragment.Callbacks {
    public static final String EXTRA_STORY_DETAILS = "EXTRA_STORY_DETAILS";

    private StoryReaderPagerMainFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sub_category_list);
        Bundle extras = getIntent().getExtras();
        fragment = (StoryReaderPagerMainFragment) getSupportFragmentManager().findFragmentByTag(StoryReaderPagerMainFragment.FRAGMENT_TAG);
        if (fragment == null && extras != null) {
            Story story = /*new Story();//*/  (Story) extras.getSerializable(EXTRA_STORY_DETAILS);
//            story.setStoryId("5403795");
            fragment = StoryReaderPagerMainFragment.newInstance(story);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container, fragment, StoryReaderPagerMainFragment.FRAGMENT_TAG);
            transaction.commit();

            setTitle(story.getTitle());
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public void setPaginator(TextViewPaginator paginator) {
        fragment.setPaginator(paginator);
    }

    @Override
    public void onChanpterClickListener(Chapter chapter) {
        fragment.loadStoryChapterFromOnline(chapter.getChapterId());
    }
}
