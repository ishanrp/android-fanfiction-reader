package com.ishan.demo.fanficreader.activities.category;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.ishan.demo.fanficreader.R;
import com.ishan.demo.fanficreader.activities.subcategory.SubCategoriesActivity;
import com.ishan.demo.fanficreader.models.Category;

/**
 * Created by ishan_000 on 31-May-15.
 */
public class CategoriesActivity extends AppCompatActivity implements CategoriesFragment.Callbacks {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories_main);
    }

    @Override
    public void onCategoryItemClick(Category category, int categoryType) {
        Intent intent = new Intent(getApplicationContext(), SubCategoriesActivity.class);
        intent.putExtra(SubCategoriesActivity.EXTRA_CATEGORY, category);
        intent.putExtra(SubCategoriesActivity.EXTRA_CATEGORY_TYPE, categoryType);
        startActivity(intent);
    }
}
