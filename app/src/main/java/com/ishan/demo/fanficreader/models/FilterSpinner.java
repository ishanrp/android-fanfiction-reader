package com.ishan.demo.fanficreader.models;

/**
 * Created by ishan_000 on 01-Jun-15.
 */
public class FilterSpinner {
    private String name;
    private String parameter;
    private int spinnerId;

    public FilterSpinner(String name, String parameter, int spinnerId) {
        this.name = name;
        this.parameter = parameter;
        this.spinnerId = spinnerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSpinnerId() {
        return spinnerId;
    }

    public void setSpinnerId(int spinnerId) {
        this.spinnerId = spinnerId;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }
}
