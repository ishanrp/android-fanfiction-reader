package com.ishan.demo.fanficreader.service;

import android.os.AsyncTask;

import com.ishan.demo.fanficreader.service.model.Request;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by ishan_000 on 31-May-15.
 */
public class AsyncNetTaskService extends AsyncTask<Request, Void, Document>{
    private AsyncTaskListener asyncTaskListener;

    public AsyncNetTaskService(AsyncTaskListener asyncTaskListener) {
        this.asyncTaskListener = asyncTaskListener;
    }

    @Override
    protected Document doInBackground(Request... params) {
        Document doc = null;
        if (params.length > 0) {
            Request request = params[0];
            Connection connection = Jsoup.connect(request.getUrl())
                    .userAgent("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
            if (request.getParams() != null) {
                connection.data(request.getParams());
            }

            try {
                switch (request.getMethod()) {
                    case GET:
                        doc = connection.get();
                        break;
                    case POST:
                        doc = connection.post();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return doc;
    }

    @Override
    protected void onPostExecute(Document document) {
        asyncTaskListener.onTaskCompletion(document);
    }

    public interface AsyncTaskListener {
        void onTaskCompletion(Document document);
    }
}
