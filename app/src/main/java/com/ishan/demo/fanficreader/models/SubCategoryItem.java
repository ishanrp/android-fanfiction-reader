package com.ishan.demo.fanficreader.models;

import java.io.Serializable;

/**
 * Created by ishan_000 on 31-May-15.
 */
public class SubCategoryItem implements Serializable{
    private int index;
    private String label;
    private String uri;
    private String count;

    public SubCategoryItem(int index, String label, String uri, String count) {
        this.index = index;
        this.label = label;
        this.uri = uri;
        this.count = count;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return label + " " + count;
    }
}
