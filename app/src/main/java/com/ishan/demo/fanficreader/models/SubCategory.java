package com.ishan.demo.fanficreader.models;

/**
 * Created by ishan_000 on 31-May-15.
 */
public class SubCategory {
    private String label;
    private String uri;

    public SubCategory(String label, String uri) {
        this.label = label;
        this.uri = uri;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
